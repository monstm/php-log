<?php

namespace Samy\Log\Interface;

use Exception;
use Psr\Log\LoggerInterface;

/**
 * Describes Log interface.
 */
interface LogInterface extends LoggerInterface
{
    /**
     * Return an instance with exception level.
     *
     * @param Exception $Exception
     * @return static
     */
    public function exception(Exception $Exception): self;

    /**
     * Return an instance with backtrace level.
     *
     * @param string $Message
     * @param int $Limit
     * @return static
     */
    public function backtrace(string $Message, int $Limit = 10): self;
}
