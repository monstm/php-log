<?php

namespace Test\Unit;

use Exception;
use Samy\Log\Abstract\AbstractLog;
use Samy\PhpUnit\AbstractTestCase as PhpUnitAbstractTestCase;

abstract class AbstractTestCase extends PhpUnitAbstractTestCase
{
    protected $log;

    public function testBacktrace(): void
    {
        $this->assertInstanceOf(AbstractLog::class, $this->log->backtrace(__CLASS__ . ": Test Backtrace"));
    }

    public function testException(): void
    {
        try {
            throw new Exception(__CLASS__ . ": Test Exception");
        } catch (Exception $Exception) {
            $this->assertInstanceOf(AbstractLog::class, $this->log->exception($Exception));
        }
    }
}
