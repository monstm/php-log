<?php

namespace Samy\Log\Abstract;

use Exception;
use Samy\Log\Interface\LogInterface;
use Samy\Psr3\Logger;

/**
 * This is a simple Log implementation that other Log can inherit from.
 */
abstract class AbstractLog extends Logger implements LogInterface
{
    /**
     * Return an instance with exception level.
     *
     * @param Exception $Exception
     * @return static
     */
    public function exception(Exception $Exception): self
    {
        $level = get_class($Exception);
        $code = $Exception->getCode();
        $message = $Exception->getMessage();
        if (!empty($code)) {
            $message .= "(" . $code . ")";
        }

        $this->log(
            $level,
            "{file}({line}): {message}",
            [
                "file" => $Exception->getFile(),
                "line" => strval($Exception->getLine()),
                "message" => $message
            ]
        );

        foreach ($Exception->getTrace() as $index => $data) {
            $this->log(
                $level . "#" . ($index + 1),
                "{file}({line}): {class}{type}{function}({arguments})",
                [
                    "file" => $data["file"] ?? "",
                    "line" => $data["line"] ?? 0,
                    "class" => $data["class"] ?? "",
                    "type" => $data["type"] ?? "",
                    "function" => $data["function"],
                    "arguments" => $this->arguments($data["args"] ?? [])
                ]
            );
        }

        return $this;
    }

    /**
     * Return an instance with backtrace level.
     *
     * @param string $Message
     * @param int $Limit
     * @return static
     */
    public function backtrace(string $Message, int $Limit = 10): self
    {
        $level = "backtrace";
        $backtrace = @debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, ($Limit + 1));

        foreach ($backtrace as $index => $data) {
            if ($index == 0) {
                $this->log(
                    $level,
                    "{file}({line}): {message}",
                    [
                        "file" => $data["file"] ?? "",
                        "line" => $data["line"] ?? 0,
                        "message" => $Message
                    ]
                );
            } else {
                $this->log(
                    $level . "#" . $index,
                    "{file}({line}): {class}{type}{function}({arguments})",
                    [
                        "file" => $data["file"] ?? "",
                        "line" => $data["line"] ?? 0,
                        "class" => $data["class"] ?? "",
                        "type" => $data["type"] ?? "",
                        "function" => $data["function"],
                        "arguments" => $this->arguments($data["args"] ?? [])
                    ]
                );
            }
        }

        return $this;
    }

    /**
     * Cast trace arguments into string.
     *
     * @param array<mixed> $arguments
     * @return string
     */
    protected function arguments(array $arguments): string
    {
        $ret = [];
        foreach ($arguments as $argument) {
            $type = strtolower(gettype($argument));
            switch ($type) {
                case "boolean":
                    array_push($ret, $type . " " . ($argument ? "true" : "false"));
                    break;
                case "integer":
                case "double":
                    array_push($ret, $type . " " . strval($argument));
                    break;
                case "string":
                    array_push($ret, $type . " \"" . $argument . "\"");
                    break;
                case "array":
                    array_push($ret, $type . " " . json_encode($argument));
                    break;
                case "object":
                    /** @phpstan-ignore-next-line */
                    array_push($ret, $type . " " . get_class($argument));
                    break;
                case "resource":
                    /** @phpstan-ignore-next-line */
                    array_push($ret, $type . " #" . get_resource_type($argument));
                    break;
                case "null":
                    array_push($ret, "null");
                    break;
                default:
                    array_push($ret, "[" . $type . "]");
                    break;
            }
        }

        return implode(", ", $ret);
    }
}
