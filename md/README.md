# PHP Log

[
	![](https://badgen.net/packagist/v/samy/log/latest)
	![](https://badgen.net/packagist/license/samy/log)
	![](https://badgen.net/packagist/dt/samy/log)
	![](https://badgen.net/packagist/favers/samy/log)
](https://packagist.org/packages/samy/log)

This is a simple way to collects and crash report everything happening in the application.

---

## Instalation

### Composer

Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

```sh
composer require samy/log
```

Please see [Composer](https://getcomposer.org/) for more information.

---

## Support

* Repository: <https://gitlab.com/monstm/php-log>
* User Manual: <https://monstm.gitlab.io/php-log/>
* Documentation: <https://monstm.alwaysdata.net/php-log/>
* Issues: <https://gitlab.com/monstm/php-log/-/issues>
