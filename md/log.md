# Log

---

## Log Instance

Simple Log Implementation.

### Syslog Instance

Implementation Syslog instance.

```php
$log = new \Samy\Log\Syslog();
```

---

## Log Interface

Describes Log interface.

### exception

Return an instance with exception level.

```php
try{
	// do something fun ...
}catch(Exception $exception){
	$log = $log->exception($exception);
}
```

### backtrace

Return an instance with backtrace level.

```php
$log = $log->backtrace($message);
```
